from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from time import sleep
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
#from faker import Faker
import unittest, time, re, datetime
import warnings
import os
#from dotenv import load_dotenv


class Scenariotest(unittest.TestCase):
    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        warnings.filterwarnings(action="ignore", message="unclosed", category=ResourceWarning)
        s=Service(r'C:\Users\Admin\Documents\PVG\chromedriver.exe')
        self.driver = webdriver.Chrome(service=s)
        self.driver.implicitly_wait(5)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_UIAutomationTest(self):
        driver = self.driver
        #wait = WebDriverWait(driver, 20)
        driver.get("https://gist.github.com/discover")
        driver.maximize_window()
        #Point 1 Login
        driver.find_element(By.XPATH, "//a[contains(text(),'Sign in')]").click()
        time.sleep(2)
        driver.find_element(By.ID, "login_field").click()
        driver.find_element(By.ID, "login_field").clear()
        driver.find_element(By.ID, "login_field").send_keys("pendekar5000@gmail.com")
        time.sleep(2)
        driver.find_element(By.ID, "password").click()
        driver.find_element(By.ID, "password").clear()
        driver.find_element(By.ID, "password").send_keys("Terminate2021")
        time.sleep(2)
        driver.find_element(By.NAME, "commit").click()
        time.sleep(2)
        #Point 2 Create Gist
        driver.find_element(By.XPATH, "(.//*[normalize-space(text()) and normalize-space(.)='pendekar5000'])[1]/preceding::*[name()='svg'][1]").click()
        time.sleep(2)
        driver.find_element(By.NAME, "gist[description]").click()
        driver.find_element(By.NAME, "gist[description]").clear()
        driver.find_element(By.NAME, "gist[description]").send_keys("ADLTesting")
        time.sleep(2)
        driver.find_element(By.NAME, "gist[contents][][name]").click()
        driver.find_element(By.NAME, "gist[contents][][name]").clear()
        driver.find_element(By.NAME, "gist[contents][][name]").send_keys("ADL First Test")
        time.sleep(2)
        driver.find_element(By.XPATH, "//div[@id='code-editor']/div/pre").click()
        driver.find_element(By.XPATH, "//div[@id='code-editor']/div/pre").clear()
        driver.find_element(By.XPATH, "//div[@id='code-editor']/div/pre").send_keys("A")
        time.sleep(2)
        driver.find_element(By.XPATH, "//form[@id='new_gist']/div/div[2]/div/button").click()
        time.sleep(2)
        #Point 3 See list Gist
        driver.find_element(By.XPATH, "//div[@id='user-links']/details/summary/img").click()
        time.sleep(2)
        driver.find_element(By.LINK_TEXT, "Your gists").click()
        time.sleep(2)
        #Point 4 Delete Gist
        driver.find_element(By.LINK_TEXT, "ADL First Test").click()
        time.sleep(2)
        driver.find_element(By.XPATH, "//main[@id='gist-pjax-container']/div/div/div/ul/li[2]/form/button").click()
        time.sleep(2)
        alert = self.driver.switch_to.alert
        alert.accept()
        time.sleep(2)
        #Point 5 LogOut
        driver.find_element(By.XPATH, "//div[@id='user-links']/details/summary/img").click()
        time.sleep(2)
        driver.find_element(By.XPATH, "//div[@id='user-links']/details/details-menu/form/button").click()
        time.sleep(2)
        driver.find_element(By.XPATH, "//input[@value='Sign out']").click()
        time.sleep(2)

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
